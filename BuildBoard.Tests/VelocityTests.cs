﻿using Microsoft.Extensions.Logging;
using BuildBoard.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using NSubstitute;
using BuildBoard.Services;
using Shouldly;
using System.Linq;

namespace BuildBoard.Tests
{
    [TestFixture]
    public class VelocityTests
    {
        private ILogger logger;
        private List<Item> testItems;
        private DateTime startFrom;

        [SetUp]
        public void Setup()
        {
            this.logger = Substitute.For<ILogger>();

            
            testItems = new List<Item>()
            {
                new Item() { Id = "Not Resolved1", ResolutionDate = null },
                new Item() { Id = "Not Resolved2", ResolutionDate = null },
                new Item() { Id = "Not Resolved3", ResolutionDate = null },
            };

            startFrom = new DateTime(2020, 5, 15, 9, 0, 0); // Friday 9am

            // Add 30 days worth of work, one ticket per day.
            for (var i = 0; i < 30; i++)
            {
                var resolutionDate = startFrom.AddDays(-i);
                if (resolutionDate.DayOfWeek == DayOfWeek.Saturday || resolutionDate.DayOfWeek == DayOfWeek.Sunday)
                    continue;

                testItems.Add(new Item()
                {
                    Id = $"Resolved{i}",
                    ResolutionDate = resolutionDate
                });
            }
        }

        [Test]
        public void ShouldCalculateVelocity()
        {
            var sut = SUT();

            var velocitySeries = sut.GetVelocitySeries(startFrom, 14, testItems)[0];

            velocitySeries.name.ShouldBe("Velocity");
            velocitySeries.data.ShouldAllBe(d => d == 10);  
        }

        [Test]
        public void ShouldCalculateVelocityOverPriorTwoWeekPeriod()
        {
            var sut = SUT();

            var velocitySeries = sut.GetVelocitySeries(new DateTime(2020, 4, 30), 14, testItems)[0];

            velocitySeries.name.ShouldBe("Velocity");
            velocitySeries.data.ShouldBe(new[] { 1d, 2d, 3d, 4d, 5d, 6d, 7d, 8d, 9d, 10d });
        }

        [Test]
        public void ShouldSkipWeekendsInSeries()
        {
            var sut = SUT();

            var velocitySeries = sut.GetVelocitySeries(startFrom, 14, testItems)[0];

            velocitySeries.name.ShouldBe("Velocity");
            velocitySeries.data.Count().ShouldBe(10); // 10 working days
            velocitySeries.data[9].ShouldBe(10);
        }

        [Test]
        public void ShouldRollUpIssuesResolvedOnWeekendsToMondays()
        {
            var sut = SUT();

            var velocitySeries = sut.GetVelocitySeries(new DateTime(2020, 5, 12), 14, testItems)[0]; // Start from Monday 11/5

            velocitySeries.name.ShouldBe("Velocity");
            velocitySeries.data[9].ShouldBe(10); // Monday

            // Now add a couple of new tickets completed on the weekend.
            testItems.Add(new Item()
            {
                Id = $"Saturday",
                ResolutionDate = new DateTime(2020, 5, 9)
            });

            testItems.Add(new Item()
            {
                Id = $"Sunday",
                ResolutionDate = new DateTime(2020, 5, 10)
            });

            velocitySeries = sut.GetVelocitySeries(new DateTime(2020, 5, 12), 14, testItems)[0]; // Start from Monday 11/5
            velocitySeries.data[9].ShouldBe(12); 
        }

        private JiraStatsService SUT()
        {
            return new JiraStatsService(logger);
        }
    }
}
