using BuildBoard.Models;
using BuildBoard.Services;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    [TestFixture]
    public class JiraStatsServiceTests
    {
        private ILogger logger;
        private List<Item> testData;
        private const string STATUS_READYFORDEV = "Ready For Dev";
        private const string STATUS_INPROGRESS = "In Progress";
        private const string STATUS_READYFOTESTING = "Ready for QA";
        private const string STATUS_TESTING = "Testing by QA";
        private const string STATUS_POCONFIRMATION = "PO Acceptance";
        private const string STATUS_DONE = "Dev Complete";

        [SetUp]
        public void Setup()
        {
            this.logger = Substitute.For<ILogger>();

            testData = new List<Item>
            {
                new Item()
                {
                    Id = "ReadyForDev",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_READYFORDEV, ChangedAt = new DateTime(2017,1,1)
                        }
                    }
                },
                new Item()
                {
                    Id = "InProgress",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_READYFORDEV, ChangedAt = new DateTime(2017,1,1)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_INPROGRESS, ChangedAt = new DateTime(2017,1,2)
                        }
                    }
                },
                new Item()
                {
                    Id = "InTesting1",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_READYFORDEV, ChangedAt = new DateTime(2017,1,1)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_INPROGRESS, ChangedAt = new DateTime(2017,1,2)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_READYFOTESTING, ChangedAt = new DateTime(2017,1,3)
                        }
                    }
                },
                new Item()
                {
                    Id = "InTesting2",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_READYFORDEV, ChangedAt = new DateTime(2017,1,1)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_INPROGRESS, ChangedAt = new DateTime(2017,1,2)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_READYFOTESTING, ChangedAt = new DateTime(2017,1,3)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_TESTING, ChangedAt = new DateTime(2017,1,4)
                        }
                    }
                },
                new Item()
                {
                    Id = "Resolved",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_READYFORDEV, ChangedAt = new DateTime(2017,1,1)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_INPROGRESS, ChangedAt = new DateTime(2017,1,2)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_READYFOTESTING, ChangedAt = new DateTime(2017,1,3)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_TESTING, ChangedAt = new DateTime(2017,1,4)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_POCONFIRMATION, ChangedAt = new DateTime(2017,1,5)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_DONE, ChangedAt = new DateTime(2017, 1, 6)
                        }
                    }
                },
                new Item()
                {
                    Id = "SkipToResolved",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_DONE, ChangedAt = new DateTime(2017,1,2)
                        }
                    }
                },
                new Item()
                {
                    Id = "BackAndForth",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_READYFORDEV, ChangedAt = new DateTime(2017,1,1)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_INPROGRESS, ChangedAt = new DateTime(2017,1,2)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_READYFOTESTING, ChangedAt = new DateTime(2017,1,3)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_TESTING, ChangedAt = new DateTime(2017,1,4)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_INPROGRESS, ChangedAt = new DateTime(2017,1,5)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_READYFOTESTING, ChangedAt = new DateTime(2017,1,6)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_TESTING, ChangedAt = new DateTime(2017,1,7)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_POCONFIRMATION, ChangedAt = new DateTime(2017,1,8)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_DONE, ChangedAt = new DateTime(2017, 1, 9)
                        }
                    }
                },
                new Item()
                {
                    Id = "Weekends",
                    History = new List<StatusChanged>()
                    {
                        new StatusChanged()
                        {
                            Status = STATUS_READYFORDEV, ChangedAt = new DateTime(2016,12,26)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_INPROGRESS, ChangedAt = new DateTime(2016,12,27)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_READYFOTESTING, ChangedAt = new DateTime(2016,12,28)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_TESTING, ChangedAt = new DateTime(2016,12,29)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_POCONFIRMATION, ChangedAt = new DateTime(2016,12,30)
                        },
                        new StatusChanged()
                        {
                            Status = STATUS_DONE, ChangedAt = new DateTime(2017, 1, 6) // 14 days, 10 business days
                        }
                    }
                }
            };
        }

        [Test]
        public void IShouldCalculateItemStats()
        {
            var sut = SUT();

            var stats = sut.GetIssueStats(testData);

            // ReadyForDev
            var itemToTest = stats.FirstOrDefault(s => s.Id == "ReadyForDev");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2017, 1, 1));
            itemToTest.InProgressOn.ShouldBe(null);
            itemToTest.InTestingOn.ShouldBe(null);
            itemToTest.InPOConfirmationOn.ShouldBe(null);
            itemToTest.ResolvedOn.ShouldBe(null);
            itemToTest.CycleTime.ShouldBe(0);

            // InProgress
            itemToTest = stats.FirstOrDefault(s => s.Id == "InProgress");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2017, 1, 1));
            itemToTest.InProgressOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.InTestingOn.ShouldBe(null);
            itemToTest.InPOConfirmationOn.ShouldBe(null);
            itemToTest.ResolvedOn.ShouldBe(null);
            itemToTest.CycleTime.ShouldBe(0);

            // InTesting1 (ready for QA).
            itemToTest = stats.FirstOrDefault(s => s.Id == "InTesting1");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2017, 1, 1));
            itemToTest.InProgressOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.InTestingOn.ShouldBe(new DateTime(2017, 1, 3));
            itemToTest.InPOConfirmationOn.ShouldBe(null);
            itemToTest.ResolvedOn.ShouldBe(null);
            itemToTest.CycleTime.ShouldBe(0);

            // InTesting2 (testing by QA).
            itemToTest = stats.FirstOrDefault(s => s.Id == "InTesting2");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2017, 1, 1));
            itemToTest.InProgressOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.InTestingOn.ShouldBe(new DateTime(2017, 1, 3)); // Should be the earliest of either qa status
            itemToTest.InPOConfirmationOn.ShouldBe(null);
            itemToTest.ResolvedOn.ShouldBe(null);
            itemToTest.CycleTime.ShouldBe(0);

            // Resolved
            itemToTest = stats.FirstOrDefault(s => s.Id == "Resolved");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2017, 1, 1));
            itemToTest.InProgressOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.InTestingOn.ShouldBe(new DateTime(2017, 1, 3));
            itemToTest.InPOConfirmationOn.ShouldBe(new DateTime(2017, 1, 5));
            itemToTest.ResolvedOn.ShouldBe(new DateTime(2017, 1, 6));
            itemToTest.CycleTime.ShouldBe(5);

            // When a step is skipped, all intermediate steps are filled.
            itemToTest = stats.FirstOrDefault(s => s.Id == "SkipToResolved");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.InProgressOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.InTestingOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.InPOConfirmationOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.ResolvedOn.ShouldBe(new DateTime(2017, 1, 2));
            itemToTest.CycleTime.ShouldBe(1);

            // BackAndForth
            itemToTest = stats.FirstOrDefault(s => s.Id == "BackAndForth");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2017, 1, 1));
            itemToTest.InProgressOn.ShouldBe(new DateTime(2017, 1, 2)); // must take in state
            itemToTest.InTestingOn.ShouldBe(new DateTime(2017, 1, 3)); // must take earliest
            itemToTest.InPOConfirmationOn.ShouldBe(new DateTime(2017, 1, 8));
            itemToTest.ResolvedOn.ShouldBe(new DateTime(2017, 1, 9));
            itemToTest.CycleTime.ShouldBe(6);

            // Weekends
            itemToTest = stats.FirstOrDefault(s => s.Id == "Weekends");
            itemToTest.ReadyForDevOn.ShouldBe(new DateTime(2016, 12, 26));
            itemToTest.InProgressOn.ShouldBe(new DateTime(2016, 12, 27));
            itemToTest.InTestingOn.ShouldBe(new DateTime(2016, 12, 28));
            itemToTest.InPOConfirmationOn.ShouldBe(new DateTime(2016, 12, 30));
            itemToTest.ResolvedOn.ShouldBe(new DateTime(2017, 1, 6));
            itemToTest.CycleTime.ShouldBe(10);
        }

        [Test]
        public void IShouldCalculateCFD()
        {
            var sut = SUT();

            var cfd = sut.GetCFDData(new DateTime(2017, 1, 8), 8, testData);

            cfd.Keys.Count().ShouldBe(5);
            cfd.Keys.ShouldNotContain("20170101"); // This is a weekend

            cfd["20170102"].ReadyForDev.ShouldBe(new string[] { "ReadyForDev" });
            cfd["20170102"].InProgress.ShouldBe(new string[] { "InProgress", "InTesting1", "InTesting2", "Resolved", "BackAndForth" });
            cfd["20170102"].Testing.ShouldBe(new string[] { });
            cfd["20170102"].POConfirmation.ShouldBe(new string[] { "Weekends" });
           // cfd["20170102"].Resolved.ShouldBe(new string[] { "SkipToResolved" });
            cfd["20170102"].CycleTime.ShouldBe(1);

            cfd["20170103"].ReadyForDev.ShouldBe(new string[] { "ReadyForDev" });
            cfd["20170103"].InProgress.ShouldBe(new string[] { "InProgress" });
            cfd["20170103"].Testing.ShouldBe(new string[] { "InTesting1", "InTesting2", "Resolved", "BackAndForth" });
            cfd["20170103"].POConfirmation.ShouldBe(new string[] { "Weekends" });
            cfd["20170103"].Resolved.ShouldBe(new string[] { "SkipToResolved" });
            cfd["20170103"].CycleTime.ShouldBe(1);

            cfd["20170104"].ReadyForDev.ShouldBe(new string[] { "ReadyForDev" });
            cfd["20170104"].InProgress.ShouldBe(new string[] { "InProgress" });
            cfd["20170104"].Testing.ShouldBe(new string[] { "InTesting1", "InTesting2", "Resolved", "BackAndForth" });
            cfd["20170104"].POConfirmation.ShouldBe(new string[] { "Weekends" });
            cfd["20170104"].Resolved.ShouldBe(new string[] { "SkipToResolved" });
            cfd["20170104"].CycleTime.ShouldBe(1);

            cfd["20170105"].ReadyForDev.ShouldBe(new string[] { "ReadyForDev" });
            cfd["20170105"].InProgress.ShouldBe(new string[] { "InProgress" });
            cfd["20170105"].Testing.ShouldBe(new string[] { "InTesting1", "InTesting2", "BackAndForth" });
            cfd["20170105"].POConfirmation.ShouldBe(new string[] { "Resolved", "Weekends" });
            cfd["20170105"].Resolved.ShouldBe(new string[] { "SkipToResolved" });
            cfd["20170105"].CycleTime.ShouldBe(1);

            cfd["20170106"].ReadyForDev.ShouldBe(new string[] { "ReadyForDev" });
            cfd["20170106"].InProgress.ShouldBe(new string[] { "InProgress" });
            cfd["20170106"].Testing.ShouldBe(new string[] { "InTesting1", "InTesting2", "BackAndForth" });
            cfd["20170106"].POConfirmation.ShouldBe(new string[] { });
            cfd["20170106"].Resolved.ShouldBe(new string[] { "Resolved", "SkipToResolved", "Weekends" });
            cfd["20170106"].CycleTime.ShouldBe(Math.Round(((double)(10 + 5 + 1)) / 3.0, 1));

            cfd.Keys.ShouldNotContain("20170107"); // This is a weekend
            cfd.Keys.ShouldNotContain("20170108"); // This is a weekend
        }

        [Test]
        public void IShouldTestCFDSeries()
        {
            var sut = SUT();

            var series = sut.GetCFDSeries(new DateTime(2017, 1, 8), 6, testData);

            // 20170102
            series[0].name.ShouldBe("Ready For Dev");
            series[0].data.Count().ShouldBe(5);
            series[0].data.ShouldBe(new List<List<object>>() {
                new List<object>() { "ReadyForDev", 1 },
                new List<object>() { "ReadyForDev", 1 },
                new List<object>() { "ReadyForDev", 1 },
                new List<object>() { "ReadyForDev", 1 },
                new List<object>() { "ReadyForDev", 1 } });

            // 20170102
            series[1].name.ShouldBe(STATUS_INPROGRESS);
            series[1].data.Count().ShouldBe(5);
            series[1].data.ShouldBe(new List<List<object>>() {
                new List<object>() { "InProgress, InTesting1, InTesting2, Resolved, BackAndForth", 5 },
                new List<object>() { "InProgress", 1 },
                new List<object>() { "InProgress", 1 },
                new List<object>() { "InProgress", 1 },
                new List<object>() { "InProgress", 1 } });

            // 20170103
            series[2].name.ShouldBe("In Testing");
            series[2].data.Count().ShouldBe(5);
            series[2].data.ShouldBe(new List<List<object>>() {
                new List<object>() { "", 0 },
                new List<object>() { "InTesting1, InTesting2, Resolved, BackAndForth", 4 },
                new List<object>() { "InTesting1, InTesting2, Resolved, BackAndForth", 4 },
                new List<object>() { "InTesting1, InTesting2, BackAndForth", 3 },
                new List<object>() { "InTesting1, InTesting2, BackAndForth", 3 } });

            // 20170104
            series[3].name.ShouldBe(STATUS_POCONFIRMATION);
            series[3].data.Count().ShouldBe(5);
            series[3].data.ShouldBe(new List<List<object>>() {
                new List<object>() { "Weekends", 1 },
                new List<object>() { "Weekends", 1 },
                new List<object>() { "Weekends", 1 },
                new List<object>() { "Resolved, Weekends", 2 },
                new List<object>() { "", 0 } });

            // 20170105
            series[4].name.ShouldBe("Resolved");
            series[4].data.Count().ShouldBe(5);
            series[4].data.ShouldBe(new List<List<object>>() {
                new List<object>() { "SkipToResolved", 1 },
                new List<object>() { "SkipToResolved", 1 },
                new List<object>() { "SkipToResolved", 1 },
                new List<object>() { "SkipToResolved", 1 },
                new List<object>() { "Resolved, SkipToResolved, Weekends", 3 } });

            // 20170106
            series[5].name.ShouldBe("Cycle Time");
            series[5].data.Count().ShouldBe(5);
            series[5].data.ShouldBe(new List<List<object>> {
                new List<object>() { 1 },
                new List<object>() { 1 },
                new List<object>() { 1 },
                new List<object>() { 1 },
                new List<object>() { Math.Round(((double)(10 + 5 + 1)) / 3.0, 1) } });
        }

        [Test]
        public void IShouldTestCycleTimeData()
        {
            var sut = SUT();

            var data = sut.GetCycleTimeData(new DateTime(2017, 1, 6), 6, testData);

            data["20170101"].Count().ShouldBe(0);

            data["20170102"][0].Name.ShouldBe("SkipToResolved");
            data["20170102"][0].StartedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170102"][0].EndedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170102"][0].CycleTime.ShouldBe(1);

            data["20170103"][0].Name.ShouldBe("SkipToResolved");
            data["20170103"][0].StartedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170103"][0].EndedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170103"][0].CycleTime.ShouldBe(1);

            data["20170104"][0].Name.ShouldBe("SkipToResolved");
            data["20170104"][0].StartedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170104"][0].EndedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170104"][0].CycleTime.ShouldBe(1);

            data["20170105"][0].Name.ShouldBe("SkipToResolved");
            data["20170105"][0].StartedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170105"][0].EndedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170105"][0].CycleTime.ShouldBe(1);

            data["20170106"][0].Name.ShouldBe("Resolved");
            data["20170106"][0].StartedOn.ShouldBe(new DateTime(2017, 1, 1));
            data["20170106"][0].EndedOn.ShouldBe(new DateTime(2017, 1, 6));
            data["20170106"][0].CycleTime.ShouldBe(5);

            data["20170106"][1].Name.ShouldBe("Weekends");
            data["20170106"][1].StartedOn.ShouldBe(new DateTime(2016, 12, 26));
            data["20170106"][1].EndedOn.ShouldBe(new DateTime(2017, 1, 6));
            data["20170106"][1].CycleTime.ShouldBe(10);

            data["20170106"][2].Name.ShouldBe("SkipToResolved");
            data["20170106"][2].StartedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170106"][2].EndedOn.ShouldBe(new DateTime(2017, 1, 2));
            data["20170106"][2].CycleTime.ShouldBe(1);
        }

        [TestCase("-2w", 7*2)]
        [TestCase("-4w", 7*4)]
        [TestCase("-12w", 7*12)]
        [TestCase("-52w", 7*52)]

        public void GetDaysByFromDate_ShouldReturnDaysFromWeeks(string weeks, int expectedResult)
        {
            var sut = SUT();

            var days = sut.GetDaysByFromDate(weeks);

            days.ShouldBe(expectedResult);
        }

        private JiraStatsService SUT()
        {
            return new JiraStatsService(logger);
        }
    }
}