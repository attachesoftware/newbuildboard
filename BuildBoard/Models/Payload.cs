﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuildBoard.Models
{
    public class Payload
    {
       // [JsonProperty("Team")]
        public string Teams { get; set; }

       // [JsonProperty("FromDate")]
        public string ReportLength { get; set; }

       // [JsonProperty("Data")]
        public string Data { get; set; }
    }
}
