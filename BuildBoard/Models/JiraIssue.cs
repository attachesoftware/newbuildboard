﻿using System;
using System.Collections.Generic;

namespace BuildBoard.Models
{
    public class Item
    {
        public string Id { get; set; }
        public DateTime? ResolutionDate { get; set; }
        public List<StatusChanged> History { get; set; }
    }

    public class StatusChanged
    {
        public DateTime ChangedAt { get; set; }
        public string Status { get; set; }
    }

    public class IssueStats
    {
        public string Id { get; set; }
        public double CycleTime { get; set; }
        public List<StatusChanged> StatusHistory { get; set; }
        public DateTime? ReadyForDevOn { get; set; }
        public DateTime? InProgressOn { get; set; }
        public DateTime? InTestingOn { get; set; }
        public DateTime? InPOConfirmationOn { get; set; }
        public DateTime? ResolvedOn { get; set; }
    }

    public class CFDItem
    {
        public DateTime Date { get; set; }

        public List<string> ReadyForDev { get; set; }

        public List<string> InProgress { get; set; }

        public List<string> Testing { get; set; }

        public List<string> POConfirmation { get; set; }

        public List<string> Resolved { get; set; }

        public double CycleTime { get; set; }

        public List<KeyValuePair<string, string>> Items { get; set; }
    }

    public class CFDSeries
    {
        public string name { get; set; }
        public IEnumerable<IEnumerable<object>> data { get; set; }
    }

    public class CycleTimeData
    {
        public string Name { get; set; }
        public DateTime? StartedOn { get; set; }
        public DateTime? EndedOn { get; set; }
        public double CycleTime { get; set; }
    }

    public class DataSeries
    {
        public string name { get; set; }
        public List<double> data { get; set; }
    }

    public class ControlChartData
    {
        public string Id { get; set; }
        public double CycleTime { get; set; }
        public double Average { get; set; }
        public double STDev { get; set; }
        public double UCL { get; set; }
        public double LCL { get; set; }

    }

}