﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuildBoard.Models
{
    public class ChartsDataResult
    {
        public bool Success { get; set; }
        public List<CFDSeries> CFDSeries { get; set; }
        public List<DataSeries> CtrlChartSeries { get; set; }
        public List<string> ChartCategories { get; set; }
        public List<DataSeries> VelocitySeries { get; set; }
        public string CFDQuery { get; internal set; }
        public string VelocityQuery { get; internal set; }
    }
}
