﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuildBoard.Services
{
    public interface IJsonFileService
    {
        Task SaveSetting(string json);
        Task<string> ReadSetting();
    }
}
