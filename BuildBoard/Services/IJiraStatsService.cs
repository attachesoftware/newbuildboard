﻿using BuildBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuildBoard.Services
{
    public interface IJiraStatsService
    {
        List<CFDSeries> GetCFDSeries(DateTime startingFrom, int daysToGoBackward, List<Item> issues);
        List<IssueStats> GetIssueStats(List<Item> issues);
        Dictionary<string, CFDItem> GetCFDData(DateTime startingFrom, int daysToGoBackward, List<Item> issues);
        List<DataSeries> GetControlChartSeries(List<Item> issues);
        Dictionary<string, List<CycleTimeData>> GetCycleTimeData(DateTime startingFrom, int daysToGoBackward, List<Item> issues);
        List<DataSeries> GetVelocitySeries(DateTime startingFrom, int daysToGoBackward, List<Item> resolvedItems);
        int GetDaysByFromDate(string fromDate);
    }
}
