﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Atlassian.Jira;
using BuildBoard.Models;
using Microsoft.Extensions.Logging;

namespace BuildBoard.Services
{
    public class JiraApiService : IJiraApiService
    {
        private ILogger logger;
        private string userName = "scrummaster";
        private string password = "SMreporting";
        private string baselineResolvedQuery = "issuetype != Feature AND issuetype != \"Sub Test Execution\" AND status != \"No Longer Required\" AND Team in ({0}) AND resolutiondate >= {1}";
        private string baselineInProgressAndDoneQuery = "issuetype != Feature AND issuetype != \"Sub Test Execution\" AND status IN ({2}) AND Team in ({0}) AND ( resolutiondate is null OR resolutiondate >= {1})";
        // was: Team IN ({0}) AND created >= '{1}' AND Type != 'Feature' AND project != ConnectMe AND Status IN ({2})

        public JiraApiService(ILogger logger)
        {
            this.logger = logger;
        }

        public async Task<(List<Item> items, string query)> GetResolvedTeamIssues(List<string> teamList, string fromDate)
        {
            var teamParam = "'" + string.Join("','", teamList) + "'";

            var jql = string.Format(baselineResolvedQuery, teamParam, fromDate);

            Jira jiraConn;

            IPagedQueryResult<Issue> issues;

            try
            {
                logger.LogInformation("Trying to create rest client...");

                jiraConn = Jira.CreateRestClient("http://jira.attache.com/", userName, password);

                logger.LogInformation("Trying to GetIssuesFromJqlAsync...");

                issues = await jiraConn.Issues.GetIssuesFromJqlAsync(jql, 500);
            }
            catch (Exception ex)
            {
                logger.LogError("Exception occurred in GetIssuesFromJqlAsync", ex);

                throw;
            }

            DateTime changedAt = DateTime.Now;
            var statuses = new List<Item>();

            return (issues.Select(i => new Item() { Id = i.JiraIdentifier, ResolutionDate = i.ResolutionDate }).ToList(), jql);
        }

        public async Task<(List<Item> items, string query)> GetTeamIssues(List<string> teamList, string fromDate, List<string> statusList)
        {
            var statusParam = "'" + string.Join("','", statusList) + "'";
            var teamParam = "'" + string.Join("','", teamList) + "'";

            var jql = string.Format(baselineInProgressAndDoneQuery, teamParam, fromDate, statusParam);

            //JiraCredentials jiraCredential = new JiraCredentials(u, p);
            Jira jiraConn;

            IPagedQueryResult<Issue> issues;

            try
            {
                logger.LogInformation("Trying to create rest client...");

                jiraConn = Jira.CreateRestClient("http://jira.attache.com/", userName, password);

                logger.LogInformation("Trying to GetIssuesFromJqlAsync...");

                issues = await jiraConn.Issues.GetIssuesFromJqlAsync(jql, 500);
            }
            catch (Exception ex)
            {
                logger.LogError("Exception occurred in GetIssuesFromJqlAsync", ex);

                throw;
            }

            DateTime changedAt = DateTime.Now;
            var statuses = new List<Item>();

            foreach (var issue in issues)
            {
                string id = issue.Key.Value;

                var jiraTicket = new Item() { Id = id };
                statuses.Add(jiraTicket);

                var changeLog = await jiraConn.Issues.GetChangeLogsAsync(id);

                jiraTicket.History = new List<StatusChanged>();
                foreach (var historyItem in changeLog)
                {
                    foreach (var change in historyItem.Items)
                    {
                        if (change.FieldName == "status")
                        {
                            jiraTicket.History.Add(
                                new StatusChanged()
                                {
                                    ChangedAt = historyItem.CreatedDate,
                                    Status = change.ToValue
                                });
                        }
                    }
                }
            }
            return (statuses, jql);

        }

        public async Task<(List<Item> items, string query)> GetOldDefectsSolvedByTeam(List<string> teamList, string fromDate, List<string> statusList, int durationDays)
        {
            var statusParam = "'" + string.Join("','", statusList) + "'";
            var teamParam = "'" + string.Join("','", teamList) + "'";

            var jql = string.Format("Team IN ({0}) AND Type = 'Defect' AND created < '{1}' AND updated >= '{1}' AND Status IN ({2})", teamParam, fromDate, statusParam);

            Jira jiraConn;

            IPagedQueryResult<Issue> issues;

            try
            {
                logger.LogInformation("Trying to create rest client...");

                jiraConn = Jira.CreateRestClient("http://jira.attache.com/", userName, password);

                logger.LogInformation("Trying to GetIssuesFromJqlAsync...");

                issues = await jiraConn.Issues.GetIssuesFromJqlAsync(jql, 500);
            }
            catch (Exception ex)
            {
                logger.LogError("Exception occurred in GetIssuesFromJqlAsync", ex);

                throw;
            }

            DateTime changedAt = DateTime.Now;
            var statuses = new List<Item>();

            foreach (var issue in issues)
            {
                string id = issue.Key.Value;

                var changeLog = await jiraConn.Issues.GetChangeLogsAsync(id);

                var inProgressStatus = statusList.FirstOrDefault(x => x.ToUpper().Contains("PROGRESS"));

                if (HasChangedToInProgressWithinCurrentSprint(changeLog, durationDays, inProgressStatus)) {

                    var jiraTicket = new Item() { Id = id };
                    statuses.Add(jiraTicket);

                    jiraTicket.History = new List<StatusChanged>();
                    foreach (var historyItem in changeLog)
                    {
                        foreach (var change in historyItem.Items)
                        {
                            if (change.FieldName == "status")
                            {
                                jiraTicket.History.Add(
                                    new StatusChanged()
                                    {
                                        ChangedAt = historyItem.CreatedDate,
                                        Status = change.ToValue
                                    });
                            }
                        }
                    }
                }
            }
            return (statuses, jql);

        }

        private bool HasChangedToInProgressWithinCurrentSprint(IEnumerable<IssueChangeLog> changeLog, int days, string inProgressStatus)
        {
            return changeLog.Where(x => x.Items.Any(i => i.ToValue == inProgressStatus)).Any(x => x.CreatedDate > DateTime.Now.AddDays(-days).Date);
        }

        public Task<List<string>> GetDefaultIssueStatuses()
        {
            return Task.FromResult(new List<string>() { "Ready for Dev", "In Progress", "Ready for QA", "Testing by QA" , "PO Acceptance" , "Release Notes", "Dev Complete" });
        }
    }
}
