﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace BuildBoard.Services
{
    public class HttpService : IHttpService
    {
        private DelegatingHandler delegateHandler;

        public HttpService()
            : this(null)
        {
        }

        public HttpService(DelegatingHandler delegateHandler)
        {
            this.delegateHandler = delegateHandler;
        }

        private HttpClient GetHttpClient(HttpMessageHandler innerHandler)
        {
            if (this.delegateHandler == null)
            {
                return new HttpClient(innerHandler);
            }
            else
            {
                this.delegateHandler.InnerHandler = innerHandler;
                return new HttpClient(this.delegateHandler);
            }
        }

        public async Task<dynamic> GetAsync(string username, string password, string uri, CookieContainer cookieContainer = null)
        {
            using (var innerHandler = new HttpClientHandler())
            {
                if (cookieContainer != null) { innerHandler.CookieContainer = cookieContainer; }

                using (var httpClient = GetHttpClient(innerHandler))
                {
                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", svcCredentials);

                    using (var response = await httpClient.GetAsync(uri).ConfigureAwait(false))
                    {
                        var responseString = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<dynamic>(responseString);
                    }
                }
            }
        }

        public async Task<dynamic> PostAsync(string username, string password, string uri, Stream data, CookieContainer cookieContainer = null)
        {
            using (var innerHandler = new HttpClientHandler())
            {
                if (cookieContainer != null) { innerHandler.CookieContainer = cookieContainer; }

                using (var httpClient = GetHttpClient(innerHandler))
                {
                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", svcCredentials);

                    using (var content = new StreamContent(data))
                    {
                        content.Headers.Add("Content-Type", "application/json");
                        using (var response = await httpClient.PostAsync(uri, content).ConfigureAwait(false))
                        {
                            var responseString = await response.Content.ReadAsStringAsync();
                            return JsonConvert.DeserializeObject<dynamic>(responseString);
                        }
                    }
                }
            }
        }

        public Stream GetDataAsStream(dynamic data)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            writer.Write(JsonConvert.SerializeObject(data));
            writer.Flush();
            stream.Position = 0;

            return stream;
        }
    }
}