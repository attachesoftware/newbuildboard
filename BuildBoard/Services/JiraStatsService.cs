﻿using BuildBoard.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BuildBoard.Services
{
    public class JiraStatsService : IJiraStatsService
    {
        private ILogger logger;

        public JiraStatsService(ILogger logger)
        {
            this.logger = logger;
        }

        public List<CFDSeries> GetCFDSeries(DateTime startFrom, int daysToGoBackward, List<Item> issues)
        {
            var itemStats = SummariseIssueStats(issues);
            var cfd = CreateCFDData(startFrom, daysToGoBackward, itemStats);

            return GenerateCFDGraphSeriesData(cfd);
        }
   
        public List<IssueStats> GetIssueStats(List<Item> issues)
        {
            return SummariseIssueStats(issues);
        }

        public Dictionary<string, CFDItem> GetCFDData(DateTime startFrom, int daysToGoBackward, List<Item> issues)
        {
            var itemStats = SummariseIssueStats(issues);

            return CreateCFDData(startFrom, daysToGoBackward, itemStats);
        }

        public int GetDaysByFromDate(string fromDate)
        {
            var resultString = Regex.Match(fromDate, @"\d+").Value;
            int days = 0;
            if (int.TryParse(resultString, out days))
                return days * 7;
            return days;
        }

        public List<DataSeries> GetVelocitySeries(DateTime startFrom, int daysToGoBackward, List<Item> resolvedItems)
        {
            var velocityDataSeries = new DataSeries() { name = "Velocity", data = new List<double>() };

            var j = 0;
            for (var i = daysToGoBackward - 1; i >= 0; i--)
            {
                var fromDate = startFrom.AddDays(-i).Date;
                var toDate = fromDate.AddDays(-14);

                if (fromDate.DayOfWeek == DayOfWeek.Saturday || fromDate.DayOfWeek == DayOfWeek.Sunday)
                    continue;

                velocityDataSeries.data.Add(resolvedItems.Count(r => r.ResolutionDate < fromDate && r.ResolutionDate >= toDate));
            }

            return new List<DataSeries>() { velocityDataSeries };
        }

        public Dictionary<string, List<CycleTimeData>> GetCycleTimeData(DateTime startFrom, int daysToGoBackward, List<Item> issues)
        {
            var itemStats = SummariseIssueStats(issues);

            var result = new Dictionary<string, List<CycleTimeData>>();
            for (var i = daysToGoBackward; i > 0; i--)
            {
                var date = startFrom.AddDays(-i).Date;
                var endOfDay = date.AddDays(1).AddHours(23).AddMinutes(59).AddSeconds(59);
                var key = endOfDay.ToString("yyyyMMdd");

                var items = new List<CycleTimeData>();

                foreach (var item in itemStats)
                {
                    if (item.ResolvedOn <= endOfDay)
                    {
                        items.Add(new CycleTimeData()
                        {
                            Name = item.Id,
                            StartedOn = item.ReadyForDevOn,
                            EndedOn = item.ResolvedOn,
                            CycleTime = item.CycleTime
                        });
                    }
                }

                var sorted = items.OrderByDescending(item => item.EndedOn).ToList();
                result.Add(key, sorted);
            }

            return result;
        }

        public List<DataSeries> GetControlChartSeries(List<Item> issues)
        {
            var itemStats = SummariseIssueStats(issues);
            var average = itemStats.Select(x => x.CycleTime).Average();
            var stdDev = CalculateStdDev(itemStats.Select(x => x.CycleTime));
            var ucl = average + (stdDev * 3);
            var lcl = average - (stdDev * 3);

            var ctrlChartData = itemStats.Select(x => new ControlChartData() {
                Id = x.Id,
                CycleTime = Math.Round((Double)x.CycleTime, 1),
                Average = Math.Round((Double)average, 1),
                UCL = Math.Round((Double)ucl, 1),
                LCL = lcl < 0 ?  0 : Math.Round((Double)lcl, 1)
            }).ToList();

            return GenerateCtrlChartGraphSeriesData(ctrlChartData);
        }


        private List<IssueStats> SummariseIssueStats(List<Item> issues)
        {
            var itemStats = new List<IssueStats>();

            DateTime changedAt = DateTime.Now;
            foreach (var issue in issues)
            {
                var statuses = issue.History;

                // get first time in dev
                var firstTimeInReadyForDev = statuses.OrderBy(i => i.ChangedAt).FirstOrDefault(i => i.Status == "Ready For Dev");

                // get last time resolved
                var itemStat = new IssueStats()
                {
                    Id = issue.Id,
                    StatusHistory = statuses,
                    ResolvedOn = statuses.OrderBy(i => i.ChangedAt).FirstOrDefault(i => i.Status == "Dev Complete" || i.Status == "Resolved")?.ChangedAt,
                    ReadyForDevOn = firstTimeInReadyForDev?.ChangedAt,
                    InProgressOn = statuses.OrderBy(i => i.ChangedAt).FirstOrDefault(i => i.Status == "In Progress")?.ChangedAt,
                    InTestingOn = statuses.OrderBy(i => i.ChangedAt).FirstOrDefault(i => i.Status == "Ready for QA" || i.Status == "Testing by QA")?.ChangedAt,
                    InPOConfirmationOn = statuses.OrderBy(i => i.ChangedAt).FirstOrDefault(i => i.Status == "PO Acceptance")?.ChangedAt,
                };

                // fill in any missing steps
                itemStat.InPOConfirmationOn = itemStat.InPOConfirmationOn ?? itemStat.ResolvedOn;
                itemStat.InTestingOn = itemStat.InTestingOn ?? itemStat.InPOConfirmationOn;
                itemStat.InProgressOn = itemStat.InProgressOn ?? itemStat.InTestingOn;
                itemStat.ReadyForDevOn = itemStat.ReadyForDevOn ?? itemStat.InProgressOn;

                var businessDays = GetBusinessDays(itemStat.ReadyForDevOn.GetValueOrDefault().Date, itemStat.ResolvedOn.GetValueOrDefault().Date);
                itemStat.CycleTime = itemStat.ResolvedOn == null ? 0 : (int)businessDays;
                itemStats.Add(itemStat);
            }

            return itemStats;
        }

        private static double GetBusinessDays(DateTime startD, DateTime endD)
        {
            double calcBusinessDays =
                1 + ((endD - startD).TotalDays * 5 -
                (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return calcBusinessDays;
        }

        private List<CFDSeries> GenerateCFDGraphSeriesData(Dictionary<string, CFDItem> cfd)
        {
            var series = new List<CFDSeries>();
            series.Add(new CFDSeries()
            {
                name = "Ready For Dev",
                data = cfd.Select(item => new List<object>() { string.Join(", ", item.Value.ReadyForDev), (double)item.Value.ReadyForDev.Count() })
            });

            series.Add(new CFDSeries()
            {
                name = "In Progress",
                data = cfd.Select(item => new List<object>() { string.Join(", ", item.Value.InProgress), (double)item.Value.InProgress.Count() })
            });

            series.Add(new CFDSeries()
            {
                name = "In Testing",
                data = cfd.Select(item => new List<object>() { string.Join(", ", item.Value.Testing), (double)item.Value.Testing.Count() })
            });

            series.Add(new CFDSeries()
            {
                name = "PO Acceptance",
                data = cfd.Select(item => new List<object>() { string.Join(", ", item.Value.POConfirmation), (double)item.Value.POConfirmation.Count() })
            });

            series.Add(new CFDSeries()
            {
                name = "Resolved",
                data = cfd.Select(item => new List<object>() { string.Join(", ", item.Value.Resolved), (double)item.Value.Resolved.Count() })
            });

            series.Add(new CFDSeries()
            {
                name = "Cycle Time",
                data = cfd.Select(item => new List<object>() { item.Value.CycleTime })
            });
            return series;
        }

        private Dictionary<string, CFDItem> CreateCFDData(DateTime startFrom, int days, List<IssueStats> itemStats)
        {
            var cfd = new Dictionary<string, CFDItem>();
            for (var i = days; i > 0; i--)
            {
                var totaCycleTime = 0d;
                var date = startFrom.AddDays(-i).Date;
                while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                {
                    i--;
                    date = startFrom.AddDays(-i).Date;
                }

                // check if we're
                if (i <= 0)
                    break;

                var endOfDay = date.AddHours(23).AddMinutes(59).AddSeconds(59);
                var key = endOfDay.ToString("yyyyMMdd");
                var cfdItem = new CFDItem()
                {
                    Date = date,
                    ReadyForDev = new List<string>(),
                    InProgress = new List<string>(),
                    Testing = new List<string>(),
                    POConfirmation = new List<string>(),
                    Resolved = new List<string>(),
                    Items = new List<KeyValuePair<string, string>>()
                };
                cfd.Add(key, cfdItem);

                foreach (var item in itemStats)
                {
                    var statusAtEndOfDay = GetLatestStatusAtEndOfDay(endOfDay, item);

                    // If status is blank, it' in a status we don't care about.
                    if (!string.IsNullOrEmpty(statusAtEndOfDay))
                    {
                        switch (statusAtEndOfDay)
                        {
                            case "Ready for Dev":
                                cfdItem.ReadyForDev.Add(item.Id);
                                break;

                            case "In Progress":
                                cfdItem.InProgress.Add(item.Id);
                                break;

                            case "Testing":
                                cfdItem.Testing.Add(item.Id);
                                break;

                            case "PO Acceptance":
                                cfdItem.POConfirmation.Add(item.Id);
                                break;

                            case "Resolved":
                                totaCycleTime += item.CycleTime;
                                cfdItem.Resolved.Add(item.Id);
                                break;
                        }

                        cfdItem.Items.Add(new KeyValuePair<string, string>(statusAtEndOfDay, item.Id));
                    }
                }

                cfdItem.CycleTime = Math.Round((Double) cfdItem.Resolved.Count() == 0 ? 0 : totaCycleTime / (double)cfdItem.Resolved.Count(), 1);

            }

            return cfd;
        }

        private string GetLatestStatusAtEndOfDay(DateTime endOfDay, IssueStats stats)
        {
            if (stats.ResolvedOn <= endOfDay)
            {
                return "Resolved";
            }
            else if (stats.InPOConfirmationOn <= endOfDay)
            {
                return "PO Acceptance";
            }
            else if (stats.InTestingOn <= endOfDay)
            {
                return "Testing";
            }
            else if (stats.InProgressOn <= endOfDay)
            {
                return "In Progress";
            }
            else if (stats.ReadyForDevOn <= endOfDay)
            {
                return "Ready for Dev";
            }

            return string.Empty;
        }

        private double CalculateStdDev(IEnumerable<double> values)
        {
            double ret = 0;
            if (values.Count() > 0)
            {
                //Compute the Average      
                double avg = values.Average();
                //Perform the Sum of (value-avg)_2_2      
                double sum = values.Sum(d => Math.Pow(d - avg, 2));
                //Put it all together      
                ret = Math.Sqrt((sum) / (values.Count() - 1));
            }
            return ret;
        }

        private List<DataSeries> GenerateCtrlChartGraphSeriesData(List<ControlChartData> ctrlChartData)
        {
            var series = new List<DataSeries>();
            series.Add(new DataSeries()
            {
                name = "Cycle Time",
                data = ctrlChartData.Select(x => x.CycleTime).ToList()
            });

            series.Add(new DataSeries()
            {
                name = "Average",
                data = ctrlChartData.Select(x => x.Average).ToList()
            });

            series.Add(new DataSeries()
            {
                name = "UCL",
                data = ctrlChartData.Select(x => x.UCL).ToList()
            });

            series.Add(new DataSeries()
            {
                name = "LCL",
                data = ctrlChartData.Select(x => x.LCL).ToList()
            });

            return series;
        }

    }
}