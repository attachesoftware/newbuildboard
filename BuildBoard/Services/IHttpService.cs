﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BuildBoard.Services
{
    public interface IHttpService
    {
        Task<dynamic> GetAsync(string username, string password, string uri, CookieContainer cookieContainer = null);
        Task<dynamic> PostAsync(string username, string password, string uri, Stream data, CookieContainer cookieContainer = null);
        Stream GetDataAsStream(dynamic data);
    }
}
