﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BuildBoard.Services
{
    public class JsonFileService : IJsonFileService
    {
        private const string SETTING_JSON_FILE = "chartsetting.json";
        public async Task SaveSetting(string json)
        {
            var fileToSave = Path.Combine(
                        Directory.GetCurrentDirectory(), SETTING_JSON_FILE);

            using (StreamWriter outputFile = new StreamWriter(fileToSave))
            {
                await outputFile.WriteAsync(json);
            }

        }

        public async Task<string> ReadSetting()
        {
            var result = ""; 
            var fileToRead = Path.Combine(
                        Directory.GetCurrentDirectory(), SETTING_JSON_FILE);

            try
            {
                using (StreamReader inputFile = new StreamReader(fileToRead))
                {
                    result = await inputFile.ReadToEndAsync();
                }
            }
            catch (Exception)
            {
            }

            return result;
        }
    }
}
