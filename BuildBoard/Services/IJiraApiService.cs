﻿using BuildBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuildBoard.Services
{
    public interface IJiraApiService
    {
        Task<(List<Item> items, string query)> GetTeamIssues(List<string> team, string fromDate, List<string> statusList);
        Task<(List<Item> items, string query)> GetOldDefectsSolvedByTeam(List<string> teamList, string fromDate, List<string> statusList, int days);
        Task<(List<Item> items, string query)> GetResolvedTeamIssues(List<string> teamList, string fromDate);
        Task<List<string>> GetDefaultIssueStatuses();
    }
}
