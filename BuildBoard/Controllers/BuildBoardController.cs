﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BuildBoard.Models;
using BuildBoard.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BuildBoard.Controllers
{   
    public class BuildBoardController : Controller
    {
        private IJiraApiService jiraApiService;
        private IJiraStatsService jiraStatsService;
        private IJsonFileService jsonFileService;
        private IMemoryCache cache;
        

        public BuildBoardController(IJiraApiService jiraApiService, IJiraStatsService jiraStatsService, IJsonFileService jsonFileService, IMemoryCache cache)
        {
            this.jiraApiService = jiraApiService;
            this.jiraStatsService = jiraStatsService;
            this.jsonFileService = jsonFileService;
            this.cache = cache;
        }

        [Route("/api/BuildBoard/GetChartsData")]
        [HttpPost]
        public async Task<IActionResult> GetChartsData([FromBody]Payload payload)
        {
            if (payload.Data == null) return Json(new ChartsDataResult() { Success = false });

            var statusList = JsonConvert.DeserializeObject<List<string>>(payload.Data);
            var teamList = JsonConvert.DeserializeObject<List<string>>(payload.Teams);
            var durationDays = jiraStatsService.GetDaysByFromDate(payload.ReportLength);

            var issues = await this.jiraApiService.GetTeamIssues(teamList, payload.ReportLength, statusList);

            //// Also includes old defects created before the sprint but assigened to team within the sprint
            //var defects = await this.jiraApiService.GetOldDefectsSolvedByTeam(teamList, payload.ReportLength, statusList, durationDays);
            //issues = issues.Union(defects).ToList();

            // Get resolved items for the duration of 
            var resolvedIssues = await this.jiraApiService.GetResolvedTeamIssues(teamList, $"-{(durationDays + 14)}d");

            if (!issues.items.Any()) return Json(new ChartsDataResult() { Success = false});

            var now = DateTime.Now.Date;

            var cfdSeries = jiraStatsService.GetCFDSeries(DateTime.Now, durationDays, issues.items);
            var ctrlChartSeries = jiraStatsService.GetControlChartSeries(issues.items);
            var ctrlChartCategories = issues.items.Select(x => x.Id).ToList();

            return Json(new ChartsDataResult()
            {
                Success = true,
                CFDSeries = cfdSeries,
                CtrlChartSeries = ctrlChartSeries,
                ChartCategories = ctrlChartCategories,
                VelocitySeries = jiraStatsService.GetVelocitySeries(now, durationDays, resolvedIssues.items),
                CFDQuery = issues.query,
                VelocityQuery = resolvedIssues.query
            });
        }

        [Route("/api/BuildBoard/GetCycleTimeData")]
        [HttpPost]
        public async Task<IActionResult> GetCycleTimeData([FromBody]Payload payload)
        {
            if (payload.Data == null) return Json(new { Success = false });

            var statusList = await this.jiraApiService.GetDefaultIssueStatuses(); 
            var teamList = JsonConvert.DeserializeObject<List<string>>(payload.Teams);
            var durationDays = jiraStatsService.GetDaysByFromDate(payload.ReportLength);

            var issues = await this.jiraApiService.GetTeamIssues(teamList, payload.ReportLength, statusList);

            // No longer required as the update GetTeamIssues covers it
            //// Also includes old defects created before the sprint but assigened to team within the sprint
            //var defects = await this.jiraApiService.GetOldDefectsSolvedByTeam(teamList, payload.ReportLength, statusList, durationDays);
            //issues = issues.items.Union(defects).ToList();

            if (!issues.items.Any()) return null;
            
            var result = jiraStatsService.GetCycleTimeData(DateTime.Now, durationDays, issues.items);

            return Json(new
            {
                Success = result.Any(x => x.Value.Count > 0),
                CycleTimeData = result.Select(r => new { Day = r.Key, Items = r.Value })
            });
        }

        [Route("/api/BuildBoard/GetIssueStatuses")]
        [HttpGet]
        public async Task<JsonResult> GetIssueStatuses()
        {
            var defaultIssueStatuses = await this.jiraApiService.GetDefaultIssueStatuses();
            var statusJson = JsonConvert.SerializeObject(defaultIssueStatuses);
            return Json(statusJson);
        }


        [Route("/api/BuildBoard/UpdateIssueStatuses")]
        [HttpPost]
        public async Task<JsonResult> UpdateIssueStatuses([FromBody] string[] statusList)
        {
            if (!statusList.Any()) return Json(new { });

            await jsonFileService.SaveSetting(JsonConvert.SerializeObject(statusList));

            return Json(statusList);
        }
    }
}